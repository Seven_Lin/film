package com.film;

import com.film.common.util.DataTimeUtil;
import com.film.dao.PosterMapper;
import com.film.pojo.Poster;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.UUID;

@SpringBootTest
public class TestPosterDao {
    @Autowired
    private PosterMapper posterMapper;

    /**
     * 查询全部海报
     */
    @Test
    void selectPoster() {
       List<Poster> list =  posterMapper.selectPoster();
        System.out.println(list);
    }
    @Test
    void selectPosterByStatus(){
        String status="1";
        List<Poster> list = posterMapper.selectByStatus(status);
        System.out.println(list);
    }
    @Test
    void insertPoster() throws FileNotFoundException {
        Poster poster = new Poster();
        //设置id
        poster.setId(UUID.randomUUID().toString());
        //设置title
        poster.setTitle("图纸");
        //设置url
        //文件的路径
        poster.setUrl("http://localhost:8888/api/upload?id=b8e76233-bf38-42fd-9030-c871b60d5cd3");
        //设置状态
        poster.setStatus("1");
        //上传时间
        String now = DataTimeUtil.getNowTimeString();
        poster.setCreateAt(now);
        posterMapper.insertPoster(poster);
    }
    @Test
    void updatePoster(){
        Poster poster = new Poster();
        poster.setTitle("图纸2");
        poster.setId("ec78b74c-56ed-4e74-ace4-80cbc154f82f");
        poster.setUrl("http://localhost:8888/api/upload?id=b8e76233-bf38-42fd-9030-c871b60d5cd3");
        poster.setStatus("1");
        String now = DataTimeUtil.getNowTimeString();
        poster.setCreateAt(now);
        posterMapper.updatePoster(poster);
    }
}
