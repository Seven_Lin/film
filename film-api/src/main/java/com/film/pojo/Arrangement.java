package com.film.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 电影的排片 播放场次 开放多少个座位
 */
@Data
@TableName("t_arrangement")
@NoArgsConstructor
@AllArgsConstructor
public class Arrangement implements Serializable {
    private String id;

    private String fid; //电影id

    private String name; //电影名

    private Integer seatNumber; //开放多少座位

    private double price; //价格

    private String type; //放映类型2D 3D

    private  String date; //电影开始时间 2012-01-10

    private String statTime;  //电影开始时间 19:30:00

    private Integer boxOffice; //票房统计

    private String endTime; //结束时间

    private String founder; //创建人

    private String createAt; //创建时间
}
