package com.film.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class LeavingMessage implements Serializable {

    private static final long serialVersionUID = 1384316584842780248L;

    private String id;

    private String uid;

    private String content;

    private String reply;

    private String createAt;
}
