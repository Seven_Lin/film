package com.film.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 电影评价
 */
@Data
@NoArgsConstructor
@TableName("t_film_evaluate")
public class FilmEvaluate implements Serializable {
    private static final long serialVersionUID = -4265332308538861976L;

    private String id;
    // 电影id
    private String fid;
    // 用户id
    private String uid;
    // 星级
    private Integer star;
    // 评语
    private String comment;
    // 创建时间
    private String createAt;
}
