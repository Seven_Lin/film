package com.film.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

/**
 * 用户
 */
@Data
@TableName("t_user")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    //用户id
    private String id;
    //用户名
    private String username;
    //用户密码
    private String password;
    //用户头像url
    private String avatar;
    //用户昵称
    private String nickname;
    //邮箱
    private String email;
    //用户出生年-月-日
    private String birthday;
    //性别
    private String gender;
    //个人简介
    private String info;
    //创建时间
    private String createAt;
    //修改时间
    private String updateAt;


}
