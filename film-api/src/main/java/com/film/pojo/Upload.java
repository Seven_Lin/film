package com.film.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 上传图片
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Upload implements Serializable {
    private static final long serialVersionUID = -1087797712214990682L;
    //文件的id
    private String id;
    //二进制图片
    private byte[] bytes;
    //文件的md5值
    private String md5;
}
