package com.film.pojo.vo;

import com.film.pojo.LeavingMessage;
import com.film.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/*
活跃留言的用户
@AllArgsConstructor 会生成一个包含所有变量，同时如果变量使用了NotNull annotation ， 会进行是否为空的校验，全部参数的构造函数的自动生成，该注解的作用域也是只有在实体类上，参数的顺序与属性定义的顺序一致。
 */
@Data
@AllArgsConstructor
public class LeavingMessageV0 implements Serializable {

    private static final long serialVersionUID = -5495689138090320647L;

    private String id;

    private LeavingMessage leavingMessage;

    private User user;
}
