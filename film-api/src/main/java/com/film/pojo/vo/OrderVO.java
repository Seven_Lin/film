package com.film.pojo.vo;

import com.film.pojo.Arrangement;
import com.film.pojo.Film;
import com.film.pojo.Order;
import com.film.pojo.User;
import lombok.Data;

/**
 * 订单输出规范
 */
@Data
public class OrderVO {

    private Order order;

    private User user;

    private Film film;

    private Arrangement arrangement;
}
