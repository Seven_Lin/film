package com.film.pojo.vo;

import com.film.pojo.Arrangement;
import com.film.pojo.Film;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 购物车前端展示
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartVO {
    //电影
    private Film film;

    //电影的排片
    private Arrangement arrangement;

    //购物车
    private Cart cart;

}
