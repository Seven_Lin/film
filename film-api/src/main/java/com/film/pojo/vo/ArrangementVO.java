package com.film.pojo.vo;

import com.film.pojo.Arrangement;
import com.film.pojo.Film;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.naming.ldap.PagedResultsControl;
import java.util.List;

@Data
@AllArgsConstructor
public class ArrangementVO {
    private List<Arrangement> arrangements;

    private Film film;
}
