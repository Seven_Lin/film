package com.film.pojo.vo;

import com.film.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActiveUserV0 {

    private User user;

    private Integer Number;
}
