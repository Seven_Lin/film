package com.film.pojo.vo;

import com.film.pojo.User;
import com.film.pojo.WorkerEvaluate;
import lombok.Data;

import java.io.Serializable;

/**
 * 员工评分前端展示
 */
@Data
public class WorkerEvaluateVO implements Serializable {
    private static final long serialVersionUID = -3543244479959919707L;

    private String id;
    private WorkerEvaluate workerEvaluate;
    private User user;
}
