package com.film.pojo.vo;

import com.film.pojo.FilmEvaluate;
import com.film.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 电影评分前端展示
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmEvaluateVO implements Serializable {
    private static final long serialVersionUID = 6877639859159386884L;

    private String id;

    private FilmEvaluate filmEvaluate;

    private User user;
}
