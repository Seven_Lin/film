package com.film.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@TableName("t_worker_evaluate")
public class WorkerEvaluate implements Serializable {
    private static final long serialVersionUID = -1382707868997452267L;

    private String id;
    // 员工ID
    private String wid;
    // 用户ID
    private String uid;
    // 评价内容
    private String content;
    // 满意 非常满意
    private String type;
    // 创建时间
    private String createAt;
}
