package com.film.pojo;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

/**
 * 订单模块
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@TableName("t_order")
public class Order implements Serializable {
    private static final long serialVersionUID = -6953157446966469928L;
    //订单id
    private String id;

    //用户uid
    private String uid;

    //电话号码
    private String phone;

    //场次aid
    private String aid;

    //座位号
    private String seats;

    //价格
    private double price;

    //订单状态
    private Integer status;

    //订单创建时间
    private String createAt;

    //订单支付时间
    private String payAt;




}
