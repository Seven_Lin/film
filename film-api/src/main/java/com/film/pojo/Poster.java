package com.film.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 首页海报
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Poster implements Serializable {
    private static final long serialVersionUID = 8457825122789991503L;
    private String id;

    private String title;

    private String url;

    //上架 下架
    private String status;

    private String createAt;
}
