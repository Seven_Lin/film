package com.film.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Activity implements Serializable {

    private static final long serialVersionUID = 5867002095977143423L;

    private String id;

    private String content;

    private int number;

    private String startTime;

    private String endTime;

    private String createAt;
}
