package com.film.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_order_exception")
public class OrderException implements Serializable {

    private static final long serialVersionUID = -2132969642244266725L;
    //订单id
    private String id;

    //订单id
    private String oid;

    //异常原因reason
    private String reason;

    //申报人、审核人
    private String reviewer;

    /*
    * 状态
    * true 已处理
    * false 待处理
    * */
    private boolean status;

    //结果
    private String result;

    //创建时间
    private String createAt;

    //处理时间
    private String endAt;


}
