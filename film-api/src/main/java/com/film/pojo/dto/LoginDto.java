package com.film.pojo.dto;

import lombok.Data;

@Data
public class LoginDto {

    //用户登陆名
    private String username;

    //用户登陆密码
    private String password;

    //记住我
    private boolean remember;

}
