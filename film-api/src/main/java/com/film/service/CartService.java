package com.film.service;

import com.film.pojo.vo.Cart;
import com.film.pojo.vo.CartVO;

import java.util.List;

public interface CartService {

    //新增到购物车
    void save(Cart cart) throws Exception;

    //基于Id删除
    void deleteById (String id);

    //全部删除
    void deleteAllByUserId(String uid);

    //查找全部
    List<CartVO> findAllByUserId(String uid);

    //删除用户选中的购物车
    void deleteCarts(List<Cart> carts);

    //结算用户选中的购物车
    void settleCarts(List<Cart> carts) throws Exception;
}
