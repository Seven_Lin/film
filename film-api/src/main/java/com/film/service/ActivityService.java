package com.film.service;

import com.film.pojo.Activity;

import java.util.List;

public interface ActivityService {

    void create(Activity activity);

    Activity findById(Integer id);

    List<Activity> findAll();

    void deleteById(Integer id);
}
