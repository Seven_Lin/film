package com.film.service;

import com.film.pojo.LeavingMessage;
import com.film.pojo.vo.ActiveUserV0;
import com.film.pojo.vo.LeavingMessageV0;

import java.util.List;

public interface LeavingMessageService {

    void save(LeavingMessage leavingMessage);

    void reply(LeavingMessage leavingMessage);

    List<LeavingMessageV0> findAll();

    List<ActiveUserV0> findActiveUsers();
}
