package com.film.service;

import com.film.pojo.Film;

import java.util.List;

public interface FilmService {
    void save(Film film);

    void deleteById(String id);

    List<Film> findAll();

    List<Film> findByRegionAndType(String region,String type);

    List<Film> findHots(Integer limit);

    List<Film> findLikeName(String name);

    Film findById(String id);

    Film update(Film film);
}
