package com.film.service.impl;

import com.film.common.util.DataTimeUtil;
import com.film.dao.ActivityMapper;
import com.film.pojo.Activity;
import com.film.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityMapper activityMapper;

    /**
     * 创建活动
     * @param activity
     */
    @Override
    public void create(Activity activity) {
        activity.setId(UUID.randomUUID().toString());//设置id，生成唯一识别码
        activity.setCreateAt(DataTimeUtil.getNowTimeString());//调用DataUtil类的getNowTimeString方法设置创建时间
    }

    /**
     * 基于id查找活动
     * @param id
     * @return
     */
    @Override
    public Activity findById(Integer id) {
        return activityMapper.findById(id);
    }

    /**
     * 查找所有活动
     * @return
     */
    @Override
    public List<Activity> findAll() {
        return activityMapper.findAll();
    }

    /**
     * 基于id删除活动
     * @param id
     */
    @Override
    public void deleteById(Integer id) {
        activityMapper.deleteById(id);
    }
}
