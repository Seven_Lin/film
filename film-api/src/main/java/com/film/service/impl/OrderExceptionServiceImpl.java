package com.film.service.impl;

import com.film.common.util.DataTimeUtil;
import com.film.dao.OrderExceptionMapper;
import com.film.pojo.OrderException;
import com.film.service.OrderExceptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class OrderExceptionServiceImpl implements OrderExceptionService {

    @Autowired
    private OrderExceptionMapper orderExceptionMapper;

    //创建异常订单
    @Override
    public OrderException create(OrderException orderException) {
        orderException.setStatus(false);
        orderException.setId(UUID.randomUUID().toString());
        orderException.setCreateAt(DataTimeUtil.getNowTimeString());
        orderExceptionMapper.insert(orderException);
        return null;
    }

    //查询所有异常订单
    @Override
    public List<OrderException> findAll() {
        return orderExceptionMapper.selectList(null);
    }

    //处理异常订单
    @Override
    public void handleException(OrderException orderException) {
        orderException.setEndAt(DataTimeUtil.getNowTimeString());
        orderExceptionMapper.updateById(orderException);
    }
}
