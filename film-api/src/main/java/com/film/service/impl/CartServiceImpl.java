package com.film.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.film.dao.ArrangementMapper;
import com.film.dao.CartMapper;
import com.film.dao.FilmMapper;
import com.film.pojo.Arrangement;
import com.film.pojo.Film;
import com.film.pojo.vo.Cart;
import com.film.pojo.vo.CartVO;
import com.film.service.ArrangementService;
import com.film.service.CartService;
import com.film.service.OrderService;
import org.springframework.cache.annotation.CacheEvict;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class CartServiceImpl implements CartService {
    @Resource
    private CartMapper cartMapper;

    @Resource
    private OrderService orderService;

    //排片
    @Resource
    private ArrangementMapper arrangementMapper;

    //电影
    @Resource
    private FilmMapper filmMapper;

    //加入购物车
    @Override
    public void save(Cart cart) throws Exception {
        cartMapper.insert(cart);
    }

    //基于Id删除
    @Override
    @CacheEvict //数据在 redis 中已经存在需要删除缓存
    public void deleteById(String id) {
        cartMapper.deleteById(id);
    }

    //基于uuid删除所有
    @Override
    @CacheEvict
    public void deleteAllByUserId(String uid) {
        cartMapper.delete(new QueryWrapper<Cart>().in("uid",uid));
    }

    //基于uuid查找全部
    @Override
    public List<CartVO> findAllByUserId(String uid) {
        List<CartVO> result = new ArrayList<>();
        List<Cart> carts = cartMapper.selectList(new QueryWrapper<Cart>().in("uid",uid));
        for (Cart c :carts ) {
            Arrangement arrangement = arrangementMapper.selectById(c.getAid());
            Film film = filmMapper.selectById(arrangement.getFid());
            //购物车展示到前端
            CartVO cartVO = new CartVO(film,arrangement,c);
            result.add(cartVO);
        }
        return result;
    }

    //删除购物车
    @Override
    public void deleteCarts(List<Cart> carts) {
        for (Cart c : carts) {
            cartMapper.deleteById(c.getId());
        }
    }

    //查询购物车
    @Override
    public void settleCarts(List<Cart> carts) throws Exception {
        for (Cart c :carts ) {
            orderService.create(c);
        }
    }
}
