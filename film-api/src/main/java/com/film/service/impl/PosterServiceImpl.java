package com.film.service.impl;

import com.film.common.util.DataTimeUtil;
import com.film.dao.PosterMapper;
import com.film.pojo.Poster;
import com.film.service.PosterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
@Service
public class PosterServiceImpl implements PosterService {
    @Autowired
    private PosterMapper posterMapper;

    /**
     * 添加海报
     * @param poster
     */
    @Override
    public void savePoster(Poster poster) {
        poster.setId(UUID.randomUUID().toString());
        poster.setStatus(DataTimeUtil.getNowTimeString());
        posterMapper.insertPoster(poster);
    }

    /**
     * 更新海报
     * @param poster
     */
    @Override
    public void updatePoster(Poster poster) {
        posterMapper.updatePoster(poster);
    }

    /**
     * 基于id删除
     * @param id
     */
    @Override
    public void deletePosterById(String id) {
        posterMapper.deletePoster(id);
    }

    /**
     * 删除全部
     */
    @Override
    public void deletePoster() {
        posterMapper.deletePosterAll();
    }

    /**
     * 查询所有海报
     * @return
     */
    @Override
    public List<Poster> selectPoster() {

        return posterMapper.selectPoster();
    }

    /**
     * 基于状态查询海报
     * @param status
     * @return
     */
    @Override
    public List<Poster> selectPosterByStatus(String status) {
        return posterMapper.selectByStatus(status);
    }
}
