package com.film.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.film.common.util.DataTimeUtil;
import com.film.constant.OrderStatus;
import com.film.dao.FilmMapper;
import com.film.dao.OrderMapper;
import com.film.dao.UserMapper;
import com.film.pojo.Arrangement;
import com.film.pojo.Film;
import com.film.pojo.Order;
import com.film.pojo.vo.Cart;
import com.film.pojo.vo.OrderVO;
import com.film.service.ArrangementService;
import com.film.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@CacheConfig(cacheNames = "order")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private FilmMapper filmMapper;

    //使用此注解,可以不实现间接口调用其他service接口
    @Resource
    private ArrangementService arrangementService;

    @Override
    public Order create(Cart cart) throws Exception {
        //通过购物车id获取购物车内已挑选的座位
        List<Integer> seats = arrangementService.getSeatsHaveSelected(cart.getAid());
        String[] split = cart.getSeats().split("号");
        //
        for (String s : split) {
            if (seats.contains(Integer.parseInt(s)))
                throw new Exception("影片在购物车中时间过长，座位已被其他用户预购");
        }
        Order order = new Order();
        //写入用户uid
        order.setUid(cart.getUid());
        //写入用户电话
        order.setPhone(cart.getPhone());
        //写入场次aid
        order.setAid(cart.getAid());
        //写入座位信息
        order.setStatus(cart.getStatus());
        order.setSeats(cart.getSeats());
        //判断订单是否已支付
        if (cart.getStatus()==2)
            order.setPayAt(DataTimeUtil.getNowTimeString());
        order.setPrice(cart.getPrice());
        order.setCreateAt(DataTimeUtil.getNowTimeString());
        orderMapper.insert(order);

        //定了几个座位添加多少热度
        Film film = filmMapper.selectById(arrangementService.findById(cart.getAid()).getFid());
        film.setHot(film.getHot()+split.length);
        filmMapper.updateById(film);
        return order;

    }

    @Override
    public Order pay(String id) throws Exception {
        Order order = orderMapper.selectById(id);
        //判断订单是否存在
        if (order==null)throw new Exception("不存在的订单id");

        //判断订单是否还在支付时间内
        if(DataTimeUtil.parseTimeStamp(order.getCreateAt())
                + OrderStatus.EXPIRATION_TIME
                < System.currentTimeMillis()){
            //超出支付时间，赋值状态码1
            order.setStatus(OrderStatus.PAYMENT_FAILED);
            orderMapper.updateById(order);
            //抛出异常
            throw new Exception("订单支付超时");
        }
        //在支付时间内，赋值状态码2
        order.setStatus(OrderStatus.PAYMENT_SUCCESSFUL);
        //订单支付时间
        order.setPayAt(DataTimeUtil.getNowTimeString());
        orderMapper.updateById(order);
        return order;
    }

    @Override
    public void update(Order order) {
        orderMapper.updateById(order);
    }

    //查询所有订单
    @Override
    public List<OrderVO> findAll() {
        return findByWrapper(new QueryWrapper<>());
    }

    //根据用户uid查询订单
    @Override
    public List<OrderVO> findByUser(String uid) {
        QueryWrapper<Order> wrapper=new QueryWrapper<>();
        wrapper.in("uid",uid);
        return findByWrapper(wrapper);
    }

    //
    private List<OrderVO> findByWrapper(QueryWrapper<Order> wrapper){
        List<Order> orders = orderMapper.selectList(wrapper);
        //添加一个存储订单数据的数组
        List<OrderVO> result =new ArrayList<>();
        //将数据库内订单所有的数据取出后，筛选出页面所需要的数据
        for (Order o : orders) {
            OrderVO orderVO = new OrderVO();
            orderVO.setOrder(o);
            orderVO.setUser(userMapper.selectById(o.getUid()));
            Arrangement arrangement = arrangementService.findById(o.getAid());
            orderVO.setArrangement(arrangement);
            orderVO.setFilm(filmMapper.selectById(arrangement.getFid()));
            result.add(orderVO);
        }
        return result;
    }
}
