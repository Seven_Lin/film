package com.film.service;

import com.film.pojo.Poster;


import java.util.List;

public interface PosterService {
    /**
     * 添加海报
     * @param poster
     */
    void savePoster(Poster poster);

    /**
     * 更新海报
     * @param poster
     */
    void updatePoster(Poster poster);

    /**
     * 基于id删除海报
     * @param id
     */
    void deletePosterById(String id);

    /**
     * 删除所有海报
     */
    void deletePoster();

    /**
     * 查询所有海报
     * @return
     */
    List<Poster> selectPoster();

    /**
     * 基于状态查询海报
     * @param status
     * @return
     */
    List<Poster> selectPosterByStatus(String status);
}
