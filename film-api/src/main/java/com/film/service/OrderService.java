package com.film.service;

import com.film.pojo.Order;
import com.film.pojo.vo.Cart;
import com.film.pojo.vo.OrderVO;

import java.util.List;

public interface OrderService {
    /**
     * 通过购物车信息创建订单
     * @param cart
     * @return
     * @throws Exception
     */
    Order create(Cart cart)throws Exception;


    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    Order pay(String id)throws Exception;


    /**
     * 更新订单信息
     * @param order
     */
    void update(Order order);


    /**
     * 查询所有订单，通过OrderVO控制返回信息
     * @return
     */
    List<OrderVO>findAll();


    /**
     * 通过uid查询该用户所有订单信息,通过OrderVO控制返回信息
     * @param uid
     * @return
     */
    List<OrderVO>findByUser(String uid);
}
