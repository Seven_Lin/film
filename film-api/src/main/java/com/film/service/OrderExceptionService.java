package com.film.service;

import com.film.pojo.OrderException;

import java.util.List;

public interface OrderExceptionService {

    /**
     * 创建异常订单
     * @param orderException
     * @return
     */
    OrderException create(OrderException orderException);


    /**
     * 查询所有异常订单
     * @return
     */
    List<OrderException> findAll();


    /**
     * 处理异常订单
     * @param orderException
     */
    void handleException(OrderException orderException);


}
