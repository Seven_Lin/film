package com.film.service;

import com.film.pojo.FilmEvaluate;
import com.film.pojo.vo.FilmEvaluateVO;

import java.util.List;

public interface FilmEvaluateService {
    // 评论电影
    void save(FilmEvaluate filmEvaluate) throws Exception;
    // 获取电影评论
    List<FilmEvaluateVO> findAllByFilmId(String fid);

    // 删除电影评分
    void deleteAllByFilmId(String fid);

    // 根据id删除评论
    void deleteById(String id);
}
