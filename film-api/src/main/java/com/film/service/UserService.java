package com.film.service;

import com.film.pojo.User;
import com.film.pojo.dto.LoginDto;

import java.util.List;

public interface UserService {
    //登录
    User login(LoginDto dto) throws Exception;

    //查找全部用户
    List<User> findAll();

    //基于id查找用户
    User findById(String id);

    //更新用户
    User update(User user);

    //新增用户
    User save(User user) throws Exception;

    //查找用户名
    User findByUsername(String name);

    //删除用户
    void deleteById(String id);


}
