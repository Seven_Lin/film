package com.film.web.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonResult implements Serializable {
    private static final long serialVersionUID = -2142126215540801243L;
    /**状态码*/
    private Integer state=1; //1表示ok,0表示Error,Experience
    /**状态信息*/
    private String message="ok";
    /**响应数据,一般为查询操作结果*/
    private Object data;

    public JsonResult(String message) {//new JsonResult("delete ok");
        this.message = message;
    }

    public JsonResult(Object data) { //new JsonResult(list);
        this.data = data;
    }

    public JsonResult(Throwable e){  //new JsonResult(exception);
//        this.state=0;
//        this.message=e.getMessage();
        this(0,e.getMessage());
    }

    public JsonResult(Integer state, String message) {
        this.state = state;
        this.message = message;
    }
}
