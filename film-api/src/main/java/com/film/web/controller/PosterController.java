package com.film.web.controller;

import com.film.pojo.Poster;
import com.film.service.PosterService;
import com.film.web.pojo.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "首页海报接口")
@RequestMapping("/film/poster")
public class PosterController {
    @Autowired
    private PosterService posterService;

    /**
     * 添加海报
     */
    @PostMapping("")
    @ApiOperation("添加海报")
    public JsonResult save(@RequestBody Poster poster){
        posterService.savePoster(poster);
        return new JsonResult("insert ok");
    }

    /**
     * 更新海报
     * @param poster
     */
    @PutMapping("")
    @ApiOperation("更新海报")
    public JsonResult update(@RequestBody Poster poster){
        posterService.updatePoster(poster);
        return new JsonResult("更新海报成功");
    }

    /**
     * 删除全部海报
     */
    @DeleteMapping("")
    @ApiOperation("删除海报")
    public JsonResult deleteAll(){
        posterService.deletePoster();
        return new JsonResult("删除成功");
    }

    /**
     * 基于id删除
     * @param id
     */
    @DeleteMapping("/{id}")
    @ApiOperation("")
    public JsonResult deleteBySatus(@PathVariable String id){
        posterService.deletePosterById(id);
        return new JsonResult("delete ok");
    }
    @GetMapping("/{status}")
    @ApiOperation("基于status查询海报")
    public JsonResult dofindPosterAll(@PathVariable String status){
        return new JsonResult(posterService.selectPosterByStatus(status)) ;
    }
    @GetMapping("")
    @ApiOperation("查询海报")
    public JsonResult selectPoster(){
        return new JsonResult(posterService.selectPoster());
    }
}
