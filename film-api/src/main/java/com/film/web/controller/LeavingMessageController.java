package com.film.web.controller;

import com.film.common.util.DataTimeUtil;
import com.film.pojo.LeavingMessage;
import com.film.pojo.vo.ActiveUserV0;
import com.film.pojo.vo.LeavingMessageV0;
import com.film.service.LeavingMessageService;
import com.film.web.pojo.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

public class LeavingMessageController {
    @Autowired
    LeavingMessageService leavingMessageService;

    /**
     * 新增留言
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @param leavingMessage
     * @return
     */
    @PostMapping("")
    @ApiOperation("新增留言")
    public JsonResult save(LeavingMessage leavingMessage){
        leavingMessageService.save(leavingMessage);
        return new JsonResult("insert ok");
    }

    /**
     * 更改留言
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @param leavingMessage
     * @return
     */
    @PutMapping("")
    @ApiOperation("更改留言")
    public JsonResult reply(LeavingMessage leavingMessage){
        leavingMessageService.reply(leavingMessage);
        return new JsonResult("更改成功");
    }

    /**
     * 查找所有留言
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @return
     */
    @GetMapping("")
    @ApiOperation("查找所有留言")
    public List<LeavingMessageV0> findAll(){
        return leavingMessageService.findAll();
    }

    /**
     * 查找留言用户
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @return
     */
    @GetMapping("")
    @ApiOperation("查找留言用户")
    public List<ActiveUserV0> findActiveUser(){
        return leavingMessageService.findActiveUsers();
    }
}
