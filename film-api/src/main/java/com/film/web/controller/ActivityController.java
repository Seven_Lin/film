package com.film.web.controller;

import com.film.pojo.Activity;
import com.film.service.ActivityService;
import com.film.web.pojo.JsonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public class ActivityController {
    @Autowired
    private ActivityService activityService;

    /**
     * 新增活动
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @param activity
     * @return
     */
    @PostMapping("")
    @ApiOperation("新增活动")
    public JsonResult save(@RequestBody Activity activity){
        activityService.create(activity);
        return new JsonResult("insert ok");
    }

    /**
     * 基于id查找活动
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @param id
     * @return
     */
    @GetMapping("{id}")
    @ApiOperation("根据id查找活动")
    public Activity findById(@RequestBody Integer id){
        return activityService.findById(id);
    }

    /**
     * 查找全部活动
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @return
     */
    @GetMapping("")
    @ApiOperation("查找全部活动")
    public List<Activity> findAll(){
        return activityService.findAll();
    }

    /**
     * 基于id删除活动
     * @ApiOperation 是用来构建Api文档的。此处为value = “接口说明”
     * @RequestBody 主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
     * @param id
     * @return
     */
    @DeleteMapping
    @ApiOperation("删除活动")
    public JsonResult deleteById(@RequestBody Integer id){
        activityService.deleteById(id);
        return new JsonResult("删除成功");
    }
}
