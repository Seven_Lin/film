package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.WorkerEvaluate;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WorkerEvaluateMapper extends BaseMapper<WorkerEvaluate> {
}
