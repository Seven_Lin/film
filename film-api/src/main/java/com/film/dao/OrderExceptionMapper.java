package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.OrderException;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderExceptionMapper extends BaseMapper<OrderException> {
}
