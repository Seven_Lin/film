package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.Arrangement;
import org.mapstruct.Mapper;

@Mapper
public interface ArrangementMapper extends BaseMapper<Arrangement> {
}
