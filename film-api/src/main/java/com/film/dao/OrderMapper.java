package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * mybatis plus
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {


}
