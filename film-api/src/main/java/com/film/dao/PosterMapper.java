package com.film.dao;

import com.film.pojo.Poster;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PosterMapper {
    /**
     * 添加首页海报
     * @param poster
     * @return
     */
    int insertPoster(Poster poster);

    /**
     * 更新海报
     * @param poster
     * @return
     */
    int updatePoster(Poster poster);

    /**
     * 基于id删除海报
     * @param id
     * @return
     */
    @Delete("delete from t_poster where id=#{id}")
    int deletePoster(String id);

    /**
     * 删除所有海报
     * @return
     */
    @Delete("delete from t_poster")
    int deletePosterAll();

    /**
     * 查询全部海报
     * @return
     */
    @Select("select  id,title,url,status,create_at from t_poster ")
    List<Poster> selectPoster();

    /**
     * 基于状态查询海报
     * @param status
     * @return
     */
//    @Select("select id,title,url,status,create_at from t_poster where status=#{status} order by create_at desc")
    List<Poster> selectByStatus(String status);





}
