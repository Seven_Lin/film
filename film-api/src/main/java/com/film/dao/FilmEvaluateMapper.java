package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.FilmEvaluate;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FilmEvaluateMapper extends BaseMapper<FilmEvaluate> {
}
