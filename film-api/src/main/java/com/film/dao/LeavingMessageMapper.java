package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.LeavingMessage;

public interface LeavingMessageMapper extends BaseMapper<LeavingMessage> {

}
