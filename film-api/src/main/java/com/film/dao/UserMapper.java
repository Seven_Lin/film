package com.film.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.User;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserMapper extends BaseMapper<User> {
}
