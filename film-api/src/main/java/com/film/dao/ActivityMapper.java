package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.Activity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ActivityMapper extends BaseMapper<Activity> {
    /**
     * 创建活动
     * @param activity
     * @return
     */

    int interActivity(Activity activity);

    /**
     * 基于id查找活动
     * @param id
     * @return
     */
    @Select("SELECT id,content,number,start_time,end_time,create_at FROM t_activity WHERE id=#{id}")
    Activity findById(Integer id);

    /**
     * 查找所有活动
     * @return
     */
    @Select("SELECT id,content,number,start_time,end_time,create_at FROM t_activity")
    List<Activity> findAll();

    /**
     * 基于id删除活动
     * @param id
     * @return
     */
    @Delete("delete from t_activity where id=#{id}")
    int delete(Integer id);
}
