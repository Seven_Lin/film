package com.film.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.film.pojo.Film;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FilmMapper extends BaseMapper<Film> {

}